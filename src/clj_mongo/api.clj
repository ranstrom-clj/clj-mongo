(ns clj-mongo.api
  (:require [again.core :as again]
            [somnium.congomongo :as cm]
            [taoensso.timbre :as log]))

(defn- deep-merge [a & maps]
  (if (map? a)
    (apply merge-with deep-merge a maps)
    (apply merge-with deep-merge maps)))

(def batch-size 500)
(def batch-sleep 1000)
(def retry-set [1000 2000 3000 10000])

(defn insert-multi! [app-db table data]
  ; TODO This should be working, but frequently hits Cosmos RU cap. Review
  (cm/with-mongo
    app-db
    (doseq [batch (partition-all batch-size data)]
      (dorun (cm/mass-insert! table batch))
      (Thread/sleep batch-sleep)))
  nil)

(defn insert! [app-db table doc]
  (log/trace "Inserting into table " table ", doc:\n" doc)
  (again/with-retries retry-set
    (cm/with-mongo app-db
      (cm/insert! table doc))))

(defn fetch-id [app-db table id value]
  (again/with-retries retry-set
    (cm/with-mongo app-db
      (cm/fetch-one table :where {id value}))))

(defn update! [app-db table doc]
  (log/trace "Executing update! into " table ", doc:\n" doc)
  (when (nil? (:_id doc))
    (throw (Exception. (str "Unable to update since :_id is missing: " doc))))
  (again/with-retries retry-set
    (cm/with-mongo
      app-db
      (cm/update! table {:_id (:_id doc)} {:$set (dissoc doc :_id)}))))

(defn add-unless-exists [app-db table doc id]
  (again/with-retries retry-set
    (cm/with-mongo app-db
      (or (cm/fetch-one table :where {id (get doc id)})
          (cm/insert! table doc)))))

(defn fetch [app-db table where]
  (log/trace "Executing fetch for table: " table ", where: " where)
  (again/with-retries retry-set
    (cm/with-mongo app-db
      (cm/fetch table :where where))))

(defn upsert! [app-db table doc id]
  (log/trace "Executing upsert into " table " with id " id ", doc:\n" doc)
  (again/with-retries retry-set
    (cm/with-mongo app-db
      (if-let [curr (cm/fetch-one table :where {id (get doc id)})]
        (cm/update! table
                    {:_id (:_id curr)}
                    {:$set (deep-merge (dissoc curr :_id) (dissoc doc :_id))})
        (cm/insert! table doc)))))

(defn max-value [app-db table k]
  ; TODO change to more performant sort/limit
  ; https://stackoverflow.com/questions/32076382/mongodb-how-to-get-max-value-from-collections
  (again/with-retries retry-set
    (-> (cm/aggregate
         table
         {:$project {:maxValue {:$max k}}}
         {:$group {:_id      nil
                   :maxValue {:$max "$maxValue"}}})
        :result first :maxValue))
  (cm/with-mongo
    app-db
    (try
      (-> (cm/aggregate
           table
           {:$project {:maxValue {:$max k}}}
           {:$group {:_id nil :maxValue {:$max "$maxValue"}}})
          :result first :maxValue)
      (catch Exception _
        nil))))

(defn clear-collection [app-db table]
  (again/with-retries retry-set
    (cm/with-mongo
      app-db
      (cm/destroy! table {}))))